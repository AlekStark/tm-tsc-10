package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.api.repository.ITaskRepository;
import com.tsc.afedorovkaritsky.tm.model.Task;

import java.util.*;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }
}
