package com.tsc.afedorovkaritsky.tm.repository;

import com.tsc.afedorovkaritsky.tm.api.repository.ICommandRepository;
import com.tsc.afedorovkaritsky.tm.constant.ArgumentConst;
import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;
import com.tsc.afedorovkaritsky.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "О разработчике"
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "О системе"
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Помощь"
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Версия"
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Список аргументов"
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Список команд"
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Создать задачу"
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Удалить все задачи"
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Показать список задач"
    );

    public static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Создать проект"
    );

    public static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Удалить все проекты"
    );

    public static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Показать список проектов"
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Выход"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, INFO, VERSION, HELP, ARGUMENTS, COMMANDS,
            TASK_CREATE, TASK_CLEAR, TASK_LIST, PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
