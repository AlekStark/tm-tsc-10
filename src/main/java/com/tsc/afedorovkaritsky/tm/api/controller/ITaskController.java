package com.tsc.afedorovkaritsky.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
