package com.tsc.afedorovkaritsky.tm.api.service;

import com.tsc.afedorovkaritsky.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
