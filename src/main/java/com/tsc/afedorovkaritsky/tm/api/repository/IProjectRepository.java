package com.tsc.afedorovkaritsky.tm.api.repository;

import com.tsc.afedorovkaritsky.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
